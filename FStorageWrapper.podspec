Pod::Spec.new do |s|
  s.name         = "FStorageWrapper"
  s.version      = "1.0.3"
  s.summary      = "FStorageWrapper is a framework created to ease testing of the FirebaseStorage framework."
  s.description  = "FStorageWrapper is a framework created to ease testing of the FirebaseStorage framework.This framework also exposes methods that you can use to download images from Firebase."
  s.homepage     = "https://bitbucket.org/LehlohonoloIsaac/fstoragewrapper/src/master"
  s.license      = "MIT"
  s.author       = { "Lehlohonolo Mbele" => "lehlohonoloisaac25@gmail.com" }
  s.social_media_url   = "http://twitter.com/LehlohonoloMbe1"
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://bitbucket.org/LehlohonoloIsaac/fstoragewrapper/src/master", :tag => "1.0.3" }
  s.source_files  = "FStorageWrapper"
end
