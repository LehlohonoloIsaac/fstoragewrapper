//
//  Result.swift
//  FStorageWrapper
//
//  Created by Lehlohonolo Mbele on 2018/04/12.
//  Copyright © 2018 Lehlohonolo Mbele. All rights reserved.
//

import Foundation

public enum Result<T> {
    case success(T)
    case failure(Error)
}

public extension Result {
    func map<U>(_ transform:(T) -> U) -> Result<U> {
        switch self {
        case .success(let val):
            return .success(transform(val))
        case .failure(let e):
            return .failure(e)
        }
    }
}

public extension Result {
    func flatMap<U>(_ transform: (T) -> Result<U>) -> Result<U> {
        switch self {
        case .success(let val):
            return transform(val)
        case .failure(let e):
            return .failure(e)
        }
    }
}

