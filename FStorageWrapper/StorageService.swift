//
//  StorageService.swift
//  FStorageWrapper
//
//  Created by Lehlohonolo Mbele on 2018/04/12.
//  Copyright © 2018 Lehlohonolo Mbele. All rights reserved.
//

import Foundation

public protocol StorageService {
    func storage() -> Reference
}

public protocol Reference {
    func child(from path: String) -> Reference
    func downloadURL(completion: @escaping (URL?, Error?) -> Void)
}
