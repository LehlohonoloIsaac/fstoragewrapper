//
//  ImageFetcher.swift
//  FStorageWrapper
//
//  Created by Lehlohonolo Mbele on 2018/04/12.
//  Copyright © 2018 Lehlohonolo Mbele. All rights reserved.
//

import Foundation

public class ImageFetcher {
    
    private var imageReference: Reference?
    
    public init(from storageService: StorageService) {
        imageReference = storageService.storage()
    }
    
    public func fetchImage(_ image: String,_ completed: @escaping (Result<URL>) -> Void) {
        imageReference?.child(from: image).downloadURL(completion: { imageURL, error in
            if let error = error {
                completed(Result.failure(error))
            } else if let imageURL = imageURL {
                completed(Result.success(imageURL))
            }
        })
    }
}
