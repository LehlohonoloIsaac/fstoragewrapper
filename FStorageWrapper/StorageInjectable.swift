//
//  StorageInjectable.swift
//  FStorageWrapper
//
//  Created by Lehlohonolo Mbele on 2018/04/12.
//  Copyright © 2018 Lehlohonolo Mbele. All rights reserved.
//

import Foundation

public protocol StorageInjectable {
    var storageService: StorageService {get}
}
